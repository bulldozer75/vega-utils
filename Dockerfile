FROM davidcaste/alpine-java-unlimited-jce:jdk
WORKDIR /opt/vega-utils
COPY . /opt/vega-utils
RUN bash gradlew shadowJar \
    && cp build/libs/vega-utils-1.0-SNAPSHOT-all.jar /opt/vega-utils/vega-utils.jar
ENTRYPOINT ["./docker-entrypoint.sh"]
