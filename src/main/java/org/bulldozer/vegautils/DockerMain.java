package org.bulldozer.vegautils;

import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

public class DockerMain extends AbstractMain {

    protected final String configPathArg = "/vega-utils.conf";
    protected final String vegaFileArg = "/tournament.vegx";

    @Argument(required = true, metaVar = "task", index = 0)
    private String taskArg;

    public DockerMain(String[] args) {
        CmdLineParser parser = new CmdLineParser(this);
        try {
            parser.parseArgument(args);
        } catch (CmdLineException e) {
            printUsage(parser, e);
            return;
        }
        setConfigPath(configPathArg);
        setVegaFile(vegaFileArg);
        setTask(taskArg);
    }

    public static void main(String[] args) {
        disableWarning();
        new DockerMain(args).run();
    }

}

