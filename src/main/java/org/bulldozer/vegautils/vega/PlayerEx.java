package org.bulldozer.vegautils.vega;

public class PlayerEx extends Tournament.Players.Player {

    int id;

    public PlayerEx(Tournament.Players.Player player, int id) {
        setBirthday(player.getBirthday());
        setFederation(player.getFederation());
        setGender(player.getGender());
        setIDNat(player.getIDNat());
        setInfo(player.getInfo());
        setKcoeffNat(player.getKcoeffNat());
        setName(player.getName());
        setOrigin(player.getOrigin());
        setRatingNat(player.getRatingNat());
        setStatus(player.getStatus());
        setStatusHistory(player.getStatusHistory());
        setTitle(player.getTitle());
        setId(id);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}