package org.bulldozer.vegautils.vega;

import java.util.HashMap;
import java.util.Map;

public class PairEx extends Tournament.Rounds.Pairing.Pair {

    final static byte TYPE_FORFEIT = 0;
    final static byte TYPE_NORMAL = 1;
    final static byte TYPE_UNRATED = 2;
    final static byte TYPE_SYSTEM_BYE = 3;
    final static byte TYPE_NOT_PAIRED_ZERO_POINT = 4;
    final static byte TYPE_NOT_PAIRED_HALF_POINT = 5;
    final static byte TYPE_NOT_PAIRED_FULL_POINT = 6;

    final static byte RESULT_0_0 = 0;
    final static byte RESULT_0_1 = 1;
    final static byte RESULT_1_0 = 10;
    final static byte RESULT_H_0 = 20;
    final static byte RESULT_H_H = 22;
    final static byte RESULT_ADJOURNED = 55;

    Tournament.PlaySystem.ScoreSystem scoreSystem;

    public PairEx(Tournament.PlaySystem.ScoreSystem scoreSystem, Tournament.Rounds.Pairing.Pair pair) {
        super();
        setBlack(pair.getBlack());
        setBoard(pair.getBoard());
        setResult(pair.getResult());
        setType(pair.getType());
        setValue(pair.getValue());
        setWhite(pair.getWhite());
        this.scoreSystem = scoreSystem;
    }

    private static final Map<Byte, String> strWhiteResult;
    static {
        strWhiteResult = new HashMap() {{
            put(RESULT_0_0, "loss");
            put(RESULT_0_1, "loss");
            put(RESULT_1_0, "win");
            put(RESULT_H_0, "draw");
            put(RESULT_H_H, "draw");
            put(RESULT_ADJOURNED, "draw"); // ???
        }};
    };

    private static final Map<Byte, String> strBlackResult;
    static {
        strBlackResult = new HashMap() {{
            put(RESULT_0_0, "loss");
            put(RESULT_0_1, "win");
            put(RESULT_1_0, "loss");
            put(RESULT_H_0, "draw");
            put(RESULT_H_H, "draw");
            put(RESULT_ADJOURNED, "draw"); // ???
        }};
    };

    public Double getPlayerScore(int playerNum) {
        if ((int)white == playerNum)
            return getScore(strWhiteResult.get(result));
        if ((int)black == playerNum)
            return getScore(strBlackResult.get(result));
        return null;
    }

    public double getWhiteScore(Byte result) {
        return getScore(strWhiteResult.get(result));
    }

    public double getBlackScore(Byte result) {
        return getScore(strBlackResult.get(result));
    }

    double getScore(String strResult) {
        if (strResult == null)
            throw new RuntimeException(String.format("Invalid result %d.", (int)result));
        switch (strResult) {
            case "loss":
                return scoreSystem.getLoss();
            case "draw":
                return scoreSystem.getDraw();
            case "win":
                return scoreSystem.getWin();
            default:
                throw new RuntimeException(String.format("Invalid result %s.", strResult));
        }
    }

    public boolean isNormal() {
        switch (type) {
            case TYPE_NORMAL:
                return true;
            default:
                return false;
        }
    }

}
