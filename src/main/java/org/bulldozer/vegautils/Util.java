package org.bulldozer.vegautils;

import org.bulldozer.vegautils.vega.Tournament;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class Util {
    public static Boolean isRunningInsideDocker() {

        try (Stream< String > stream = Files.lines(Paths.get("/proc/1/cgroup"))) {
            return stream.anyMatch(line -> line.contains("/docker"));
        } catch (IOException e) {
            return false;
        }
    }

    public static Tournament unmarshallTournament(String vegaFile) {
        JAXBContext jc = null;
        Tournament tournament = null;
        try {
            jc = JAXBContext.newInstance( "org.bulldozer.vegautils.vega" );
            Unmarshaller u = jc.createUnmarshaller();
            tournament = (Tournament) u.unmarshal( new FileInputStream( vegaFile ) );
        } catch (JAXBException | FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        return tournament;
    }

}
