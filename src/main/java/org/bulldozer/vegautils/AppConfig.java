package org.bulldozer.vegautils;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigParseOptions;

import java.io.File;

public class AppConfig {

    private Config defaultConfig;
    private Config fileConfig;
    private Config actualConfig;
    //private ConfigBean bean;

    private static AppConfig instance;

    public static Config get() {
        return instance.actualConfig;
    }

    public static void createConfig(String configPath) {
        instance = new AppConfig();
        instance.defaultConfig = ConfigFactory.parseResources("default.conf", ConfigParseOptions.defaults());
        instance.fileConfig = ConfigFactory.parseFile(new File(configPath), ConfigParseOptions.defaults());
        instance.actualConfig = ConfigFactory.defaultOverrides()
                .withFallback(instance.fileConfig)
                .withFallback(instance.defaultConfig);
    }
}
