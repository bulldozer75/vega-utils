package org.bulldozer.vegautils;

public class Log {
    public static void print(boolean verbose, String str) {
        if (verbose ==false || AppConfig.get().getBoolean("verbose"))
            System.out.printf(str);
    }
}