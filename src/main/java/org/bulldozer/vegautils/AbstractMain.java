package org.bulldozer.vegautils;

import com.typesafe.config.Config;
import org.bulldozer.vegautils.iscore.TaskImprovementScore;
import org.bulldozer.vegautils.vega.Tournament;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

public class AbstractMain {

    protected String configPath;
    protected String vegaFile;
    protected String task;

    public void setConfigPath(String configPath) {
        this.configPath = configPath;
    }

    public void setVegaFile(String vegaFile) {
        this.vegaFile = vegaFile;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public static void disableWarning() {
        System.err.close();
        System.setErr(System.out);
    }

    void run() {

        AppConfig.createConfig(configPath);
        Config taskConfig;

        Tournament tournament = Util.unmarshallTournament(vegaFile);
        if (tournament == null)
            return;

        if (task != null && task.equalsIgnoreCase("iscore")) {
            taskConfig = AppConfig.get().getConfig("tasks.iscore");
            new TaskImprovementScore(taskConfig, tournament).run();
        }
        else if (task != null && task.equalsIgnoreCase("test")) {
            taskConfig = AppConfig.get().getConfig("tasks.test");
            new TaskTest(taskConfig, tournament).run();
        }
        else {
            Log.print(false, "Unknown task.\n");
            printUsage(null, null);
            return;
        }
    }

    void printUsage(CmdLineParser parser, CmdLineException e) {
        if (e != null)
            Log.print(false, String.format("%s\n", e.getMessage()));
        if (Util.isRunningInsideDocker())
            Log.print(false, String.format("Usage: docker run --rm -it vega-utils [options...] <task> <vegaFile>\n"));
        else
            Log.print(false, String.format("Usage: java -jar vega-utils.jar [options...] <task> <vegaFile>\n"));
        if (parser != null)
            parser.printUsage(System.err);
        Log.print(false, "Task list:\n");
        Log.print(false, "1) iscore (calculates the improvement scores)\n");
        Log.print(false, "2) test (doesn't do anything useful)\n");
        if (Util.isRunningInsideDocker())
            Log.print(false, String.format("Example: docker run --rm -it vega-utils iscore tournament.vegx\n"));
        else
            Log.print(false, String.format("Example: java -jar vega-utils.jar iscore tournament.vegx\n"));
    }
}

