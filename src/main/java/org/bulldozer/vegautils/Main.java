package org.bulldozer.vegautils;

import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

public class Main extends AbstractMain {

    @Option(name = "-c", usage = "config file path")
    private String configPathArg = "vega-utils.conf";

    @Argument(required = true, metaVar = "task", index = 0)
    private String taskArg;

    @Argument(required = true, metaVar = "vegaFile", index = 1)
    private String vegaFileArg;

    public Main(String[] args) {
        CmdLineParser parser = new CmdLineParser(this);
        try {
            parser.parseArgument(args);
        } catch (CmdLineException e) {
            printUsage(parser, e);
            return;
        }
        setConfigPath(configPathArg);
        setVegaFile(vegaFileArg);
        setTask(taskArg);
    }

    public static void main(String[] args) {
        disableWarning();
        new Main(args).run();
    }

}

