package org.bulldozer.vegautils.iscore;

public class ImprovementScore {
    boolean eligible;
    double score;
    int nGames;

    public boolean isEligible() {
        return eligible;
    }

    public void setEligible(boolean eligible) {
        this.eligible = eligible;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public int getnGames() {
        return nGames;
    }

    public void setnGames(int nGames) {
        this.nGames = nGames;
    }

    public Double getMeanScore() {
        if (nGames > 0)
            return score / nGames;
        else
            return null;
    }
}
