package org.bulldozer.vegautils.iscore;

import org.bulldozer.vegautils.vega.PlayerEx;
import org.bulldozer.vegautils.vega.Tournament;

import java.util.Comparator;

public class PlayerISEx extends PlayerEx {

    ImprovementScore improvementScore;

    public PlayerISEx(PlayerEx playerEx) {
        super(playerEx, playerEx.getId());
        setImprovementScore(new ImprovementScore());
    }

    public PlayerISEx(Tournament.Players.Player player, int id) {
        super(player, id);
        setImprovementScore(new ImprovementScore());
    }

    public ImprovementScore getImprovementScore() {
        return improvementScore;
    }

    public void setImprovementScore(ImprovementScore improvementScore) {
        this.improvementScore = improvementScore;
    }

    static class PlayerISExComparatorByMeanImprovementScore implements Comparator<PlayerISEx> {
        @Override
        public int compare(PlayerISEx o1, PlayerISEx o2) {
            double s1 = o1.getImprovementScore().getMeanScore();
            double s2 = o2.getImprovementScore().getMeanScore();
            if (s1 > s2)
                return -1;
            else if (s1 < s2)
                return +1;
            else
                return 0;
        }
    }
}
