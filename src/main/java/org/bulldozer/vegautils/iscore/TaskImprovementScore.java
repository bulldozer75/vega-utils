package org.bulldozer.vegautils.iscore;

import com.typesafe.config.Config;
import org.bulldozer.vegautils.Log;
import org.bulldozer.vegautils.vega.PairEx;
import org.bulldozer.vegautils.vega.Tournament;

import java.util.ArrayList;
import java.util.List;

public class TaskImprovementScore {

    Config config;
    Tournament tournament;

    public TaskImprovementScore(Config config, Tournament tournament) {
        this.config = config;
        this.tournament = tournament;
    }

    public void run() {

        final Tournament.PlaySystem.ScoreSystem scoreSystem = tournament.getPlaySystem().getScoreSystem();
        final Float totalScoreWinLoss = scoreSystem.getWin() + scoreSystem.getLoss();
        final Float totalScoreDrawDraw = scoreSystem.getDraw() + scoreSystem.getDraw();
        final Float maxTotalScore = Math.max(totalScoreWinLoss, totalScoreDrawDraw);

        List<PlayerISEx> players = new ArrayList<>();
        for (int i = 0; i < tournament.getPlayers().getPlayer().size(); i++) {
            PlayerISEx player = new PlayerISEx(tournament.getPlayers().getPlayer().get(i), i + 1);
            players.add(player);
            player.getImprovementScore().setEligible(getPlayerRating(player) != null);
        }

        Log.print(true, "\n");
        for (PlayerISEx player : players) {
            Double rating = getPlayerRating(player);
            Log.print(true, String.format("------- %s (%.0f) --------\n", player.getName(), rating != null ? rating : 0));
            if (player.getImprovementScore().isEligible() == false) {
                Log.print(true, "Ineligible.\n");
                continue;
            }
            calcImprovementScoreForPlayer(tournament, scoreSystem, maxTotalScore, players, player);
            double impScore = player.getImprovementScore().getScore();
            int nGames = player.getImprovementScore().getnGames();
            double meanImpScore = impScore / (double) nGames;
            int nRounds = tournament.getRounds().getPairing().size();
            boolean eligible = (double)nGames / (double)nRounds >= config.getDouble("eligibility.minRatedGamesRate");
            player.getImprovementScore().setEligible(eligible);
            Log.print(true, String.format("improvement score = %f, rated games = %d, eligibility criteria met = %s, mean improvement score = %f\n",
                    impScore,
                    nGames,
                    eligible ? "yes" : "no",
                    meanImpScore));
            Log.print(true, "\n");
        }

        List<PlayerISEx> unsortedEligiblePlayers = new ArrayList<>();
        List<PlayerISEx> ineligiblePlayers = new ArrayList<>();
        for (PlayerISEx player : players) {
            if (player.getImprovementScore().isEligible())
                unsortedEligiblePlayers.add(player);
            else
                ineligiblePlayers.add(player);
        }
        List<PlayerISEx> sortedEligiblePlayers = new ArrayList<>(unsortedEligiblePlayers);
        sortedEligiblePlayers.sort(new PlayerISEx.PlayerISExComparatorByMeanImprovementScore());
        Log.print(false, "\n");
        Log.print(false, "------------ Mean Improvement Scores (Sorted) ---------------\n");
        int p = 1;
        for (PlayerISEx player : sortedEligiblePlayers) {
            Double meanScore = player.getImprovementScore().getMeanScore();
            Log.print(false, String.format("%d. %s %f\n", p, player.getName(), meanScore != null ? meanScore : 0.));
            p++;
        }
        Log.print(false, "\n");
        Log.print(false, "Ineligible players:\n");
        for (PlayerISEx player : ineligiblePlayers) {
            Double meanScore = player.getImprovementScore().getMeanScore();
            Log.print(false, String.format("%d. %s %f\n", p, player.getName(), meanScore != null ? meanScore : 0.));
            p++;
        }
    }

    Double getPlayerRating(Tournament.Players.Player player) {
        Double rating = null;
        // Always use the national rating.
        if (player.getRatingNat() != null && player.getRatingNat() != 0)
            rating = (double) player.getRatingNat();
        return rating;
    }

    void calcImprovementScoreForPlayer(Tournament tournament, Tournament.PlaySystem.ScoreSystem scoreSystem, Float maxTotalScore,
                                       List<PlayerISEx> players, PlayerISEx player) {
        Double rating = getPlayerRating(player);
        if (rating == null) {
            Log.print(true, String.format("Player %s is not rated.\n", player.getName()));
            return;
        }
        List<Tournament.Rounds.Pairing> pairings = tournament.getRounds().getPairing();
        int roundNum = 1;
        for (Tournament.Rounds.Pairing pairing : pairings) {
            List<Tournament.Rounds.Pairing.Pair> pairs = pairing.getPair();
            boolean paired = false;
            for (Tournament.Rounds.Pairing.Pair pair: pairs) {
                PairEx pairEx = new PairEx(scoreSystem, pair);
                if ((int)pairEx.getWhite() != player.getId() && (int)pairEx.getBlack() != player.getId())
                    continue;
                int oppNum = 0;
                if ((int)pairEx.getWhite() == player.getId())
                    oppNum = (int)pairEx.getBlack();
                else if ((int)pairEx.getBlack() == player.getId())
                    oppNum = (int)pairEx.getWhite();
                PlayerISEx opponent = null;
                opponent = oppNum > 0 ? players.get(oppNum - 1) : null;
                if (pairEx.isNormal() == false) {
                    if (opponent != null)
                        Log.print(true, String.format("Round %d vs %s: game not rated.\n", roundNum, opponent.getName()));
                    else
                        Log.print(true, String.format("Round %d: no rated game.\n", roundNum));
                    continue;
                }
                Double oppRating = getPlayerRating(opponent);
                if (oppRating == null) {
                    Log.print(true, String.format("Round %d vs %s: opponent is not rated.\n", roundNum, opponent.getName()));
                    continue;
                }
                double realScore = pairEx.getPlayerScore(player.getId());
                double normalizedRealScore = realScore / maxTotalScore;
                double expectedScore = EloRating.expectedScore(rating - oppRating);
                double addition = normalizedRealScore - expectedScore;
                ImprovementScore is = player.getImprovementScore();
                is.setScore(is.getScore() + addition);
                is.setnGames(is.getnGames() + 1);
                Log.print(true, String.format("Round %d vs %s: %f - %f = %f.\n", roundNum, opponent.getName(), normalizedRealScore, expectedScore, addition));
            }
            roundNum++;
        }
    }

}

