package org.bulldozer.vegautils.iscore;

public class EloRating {
    public static double expectedScore(double ratingDifference) {
        return 1. / (1. + Math.pow(10., -ratingDifference / 400.));
    }
}
