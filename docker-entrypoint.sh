#!/bin/sh
set -e
exec java ${JVM_OPTS} -cp /opt/vega-utils/vega-utils.jar org.bulldozer.vegautils.DockerMain $1 $2 $3 $4 $5 $6 $7 $8 $9
